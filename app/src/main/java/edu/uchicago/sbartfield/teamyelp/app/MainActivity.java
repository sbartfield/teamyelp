package edu.uchicago.sbartfield.teamyelp.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

//////////////
//see http://stackoverflow.com/questions/6676733/yelp-integration-in-android
//^^this has been done
//////////////

public class MainActivity extends ActionBarActivity {


    private String mYelpResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button searchBtn = (Button) findViewById(R.id.searchBtn);

        //Calculate currency when the button is clicked
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get editText query and pass to Results activity
                EditText et = (EditText) findViewById(R.id.editText);
                EditText ct = (EditText) findViewById(R.id.city);


                String query=et.getText().toString();
                String city = ct.getText().toString();
                Intent i = new Intent(MainActivity.this,ResultsActivity.class);
                i.putExtra("query",query);
                i.putExtra("city",city);
                startActivity(i);
            }
        });
    }




}
