package edu.uchicago.sbartfield.teamyelp.app;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


public class ResultsActivity extends ListActivity {

    private String mYelpResponse = "";
    ArrayList<String> mListItems=new ArrayList<String>();
    ArrayAdapter<String> mAdapter;
    String mQuery;
    String mCity;
    private JSONArray mJS;
    JSONObject mJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        mQuery= b.get("query").toString();
        mCity = b.get("city").toString();

        mAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mListItems);
        setListAdapter(mAdapter);

        //getYelpData
        getYelpData f =new getYelpData();
        f.execute();
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        String placeName=(String)getListAdapter().getItem(position);

        for (int i = 0; i < mJS.length(); i++) {
            try {
                JSONObject entry = mJS.getJSONObject(i);
                if(entry.getString("name").equals(placeName)) {
                    Intent sfdi = new Intent(ResultsActivity.this,DetailsActivity.class);
                    sfdi.putExtra("name",placeName);
                    sfdi.putExtra("phone",entry.getString("display_phone"));
                    sfdi.putExtra("url",entry.getString("url"));
                    sfdi.putExtra("rating_url",entry.getString("rating_img_url_large"));

                    JSONObject mJA=entry.getJSONObject("location");
                    String jStr=mJA.getString("address");
                    sfdi.putExtra("address",jStr);

                    startActivity(sfdi);
                }



            }
            catch (JSONException e){
                Log.e("ERROR:", "JSON PARSING ERROR");
            }
        }



    }



    private class getYelpData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {

                String consumerKey = "lP2sfgEdP2Fedv1CgXvCYw";
                String consumerSecret = "phISngY1Q_TEs4qLDxmTcP55tS0";
                String token = "pi0SgGx7tORQbmu60F9HLI92erpQWWMy";
                String tokenSecret = "WozE0G0JTY8FYmD7DYX3rORDzLc";

                Yelp yelp = new Yelp(consumerKey, consumerSecret, token, tokenSecret);
                //String response = yelp.search(mQuery, 30.361471, -87.164326);
                //String response = yelp.search(mQuery, 30.361471, -87.164326);
                String response = yelp.search(mQuery, mCity);

                Log.i("!!!!!", response);
                mYelpResponse = response;


//               JSONObject jsonObj = new JSONObject(response);
//               mYelpResponse = jsonObj.toString();

                //Getting JSON Array node
                //currencyRates = jsonObj.getJSONObject("");

                //} catch (JSONException e) {
            } catch (Exception e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                mJson = new JSONObject(mYelpResponse);
                //json=json.getJSONObject("businesses");


                mJS=mJson.getJSONArray("businesses");

                for (int i = 0; i < mJS.length(); i++) {
                    JSONObject entry=mJS.getJSONObject(i);
                    mListItems.add(entry.getString("name"));
                }


            }
            catch (JSONException e){
                Log.e("ERROR:", "JSON PARSING ERROR");
            }

            mAdapter.notifyDataSetChanged();
        }

    }
}
