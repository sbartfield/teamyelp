package edu.uchicago.sbartfield.teamyelp.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.net.URL;


public class DetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        String name=b.get("name").toString();
        String phone=b.get("name").toString();
        String address=b.get("address").toString();
        String rurl=b.get("url").toString();

        String rating=b.get("rating_url").toString();



        TextView tvname= (TextView) findViewById(R.id.name);
        TextView tvphone= (TextView) findViewById(R.id.phone);
        TextView tvaddress= (TextView) findViewById(R.id.address);
        TextView tvurl= (TextView) findViewById(R.id.url);

        tvname.setText("Name: "+name);
        tvphone.setText("Phone: "+ phone);
        tvurl.setText("URL: "+rurl);
        tvaddress.setText("Address: "+address);

        WebView wv= (WebView) findViewById(R.id.webView);
        wv.loadUrl(rating);


    }



}
